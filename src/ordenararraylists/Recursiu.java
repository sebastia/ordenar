/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenararraylists;

/**
 *
 * @author Sebastià
 */
public class Recursiu {

    public static int factorial(int n) throws Exception {

        if (n < 0) {
            throw new Exception();
        }
            if (n <= 1) {
                return 1;
            } else {
                return n * factorial(n - 1);
            }
        
    }
    public static int fibonacci (int n) throws Exception{
        if (n<0) {
            throw new Exception();
        }
        if (n<=1) {
            return 1;
        } else {
            return fibonacci(n-1)+fibonacci(n-2);
        }
    }
    public static int binomial (int n, int k) throws Exception {
        if ((n<0)||(k<0)){
            throw new Exception();
            
        } if (k==0){
            return 1;
        } else if (n==k) {
            return 1;
        } else {
            return binomial(n-1,k)+binomial(n-1,k-1);
        }
    }
}
