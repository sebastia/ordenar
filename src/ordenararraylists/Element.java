/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ordenararraylists;

/**
 *
 * @author Sebastià
 */
public class Element {
    private int clau;

    public Element(int clau) {
        this.clau = clau;
    }

    public int getClau() {
        return clau;
    }

    public void setClau(int clau) {
        this.clau = clau;
    }
    
}
