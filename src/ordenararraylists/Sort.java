/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ordenararraylists;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Sebastià
 */
public class Sort {

    private ArrayList<Element> array;
    private static final int MAX = 10;

    public int getArray() {
        return array.size();
    }

    public Sort(int n) {
        array = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            array.add(new Element((int) (Math.round(Math.random() * MAX))));
        }
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < array.size(); i++) {
            str += array.get(i).getClau() + "\n";
        }
        return str;
    }

    public void insercioDirecte() {
        for (int i = 1; i < array.size(); i++) {
            Element e = array.get(i);
            int j = i - 1;
            while ((j >= 0) && (e.getClau() < array.get(j).getClau())) {
                array.set(j + 1, array.get(j));
                j--;
            }
            array.set(j + 1, e);
        }
    }

    public void seleccioDirecte() {
        for (int i = 0; i < array.size() - 1; i++) {
            Element e = array.get(i);
            int k = i;
            for (int j = i + 1; j < array.size(); j++) {
                if (array.get(j).getClau() < e.getClau()) {
                    e = array.get(j);
                    k = j;
                }

            }

            array.set(k, array.get(i));
            array.set(i, e);
        }
    }

    public void bimbolla() {
        for (int i = 1; i < array.size(); i++) {
            for (int j = array.size() - 1; j >= i; j--) {
                if (array.get(j).getClau() < array.get(j - 1).getClau()) {
//                      Element e = array.get(j);
//                      array.set(j,array.get(j-1));
//                      array.set(j-1, e);
                    Collections.swap(array, j, j - 1);
                }
            }
        }
    }

    public void totxana() {
        for (int i = array.size() - 2; i > 0; i--) {
            for (int j = 0; j <= i; j++) {
                if (array.get(j).getClau() > array.get(j + 1).getClau()) {
//                      Element e = array.get(j);
//                      array.set(j,array.get(j-1));
//                      array.set(j-1, e);
                    Collections.swap(array, j, j + 1);
                }
            }
        }
    }

    public void espolsada() {
        int esq = 1;
        int dret = array.size() - 1;
        int k = dret;
        while (esq <= dret) {
            for (int i = dret; i >= esq; i--) {
                if (array.get(i).getClau() < array.get(i - 1).getClau()) {
                    Collections.swap(array, i, i - 1);
                    k = i;
                }

            }
            esq = k + 1;
            for (int i = esq; i <= dret; i++) {
                if (array.get(i - 1).getClau() > array.get(i).getClau()) {
                    Collections.swap(array, i, i - 1);
                    k = i;
                }
            }
            dret = k - 1;
        }
    }

    public void cabas() {
        ArrayList<Integer> cabassos = new ArrayList<>();
        while (cabassos.size() < MAX + 1) {
            cabassos.add(0);
        }
        for (int i = 0; i < array.size(); i++) {
            int clau = array.get(i).getClau();
            cabassos.set(clau, cabassos.get(clau) + 1);
        }
        array.clear();
        for (int i = 0; i < cabassos.size(); i++) {
            for (int j = 0; j < cabassos.get(i); j++) {
                array.add(new Element(i));
            }
        }
    }

    public void fusioRecursiu(int inf, int sup) {
        if ((sup - inf + 1) <= 1) {

        } else if ((sup - inf + 1) == 2) {
            if (array.get(inf).getClau() > array.get(sup).getClau()) {
                Collections.swap(array, inf, sup);
            }
        } else {
            int mig = inf + (sup - inf) / 2;

            fusioRecursiu(inf, mig);
            fusioRecursiu(mig + 1, sup);
            combinar(inf, mig, sup);
        }
    }

    private void combinar(int inf, int mig, int sup) {
        ArrayList<Element> b = new ArrayList<>();
        int i = inf;
        int j = mig + 1;
        int k = 0;
        while ((i <= mig) && (j <= sup)) {
            if (array.get(i).getClau() > array.get(j).getClau()) {
                b.add(k, array.get(j));
                j++;
            } else {
                b.add(k, array.get(i));
                i++;
            }
            k++;
        }
        while (i <= mig) {
            b.add(k++, array.get(i));
            i++;
        }
        while (j <= sup) {
            b.add(k++, array.get(j));
            j++;
        }
        for (int l = 0; l < b.size(); l++) {
            array.set(inf + l, b.get(l));
        }
    }

    public int binary(int clau) {
        int esq = 0;
        int dret = array.size() - 1;
        while (dret > esq) {
            int mig = (dret - esq) / 2;
            if (array.get(mig).getClau() == clau) {
                return mig;
            } else if (array.get(mig).getClau() < clau) {
                esq = mig + 1;
            } else {
                dret = mig - 1;
            }
        }

        return -1;
    }
    public int binariRecursiu (int esq, int dret,int clau){
        int mig = (dret-esq)/2;
        if (esq==dret){
            return -1;
        } else if (array.get(mig).getClau()==clau){
            return mig;
        } else if (array.get(mig).getClau()>clau){
            return binariRecursiu(esq, mig-1, clau);
        } else {
            return binariRecursiu(mig+1, dret, clau);
        } 
        
    }

}
